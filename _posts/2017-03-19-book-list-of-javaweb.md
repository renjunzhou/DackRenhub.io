---
layout: post
title: Book List of JAVA Web
---
# 0. Language
- 0.1 Think in Java
- 0.2 Effective...
- 0.3 Core...

# 1. Overview
- 1.1 Professional Java for Web Applications
- 1.2 Client-Server Web Apps with JavaScript andJava （全端Web开发：使用JavaScript与Java）

# 2. Network and Operator System
- 2.1
- 2.2

# 3. Framework
- 3.1 Spring In Action
- 3.2 Spring Boot In Action (JavaEE开发的颠覆者 Spring Boot实战)

# 4. Support
- 4.1 Maven
- 4.2 Git
- 4.3 Junit

# 5. Theory
