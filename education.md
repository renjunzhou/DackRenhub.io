---
layout: default
title: Education
---

# Software architecture and System integration, M.E 2016 - Present
## Conservatoire National des Arts et Métiers

- Java Software Architectures
- System Architecture
- Project Engineering
- Android & Java card

# Computer Technology, M.E 2015 - 2018
## Beijing University of Technology
- Object - oriented design
- Software engineering
- Software architecture
- Artificial intelligence
- Distribute system

# Computer Science, B.S 2011 - 2015
## Shanxi Agricultural University
- Computer structure
- Data structure
- Network
- Operator system
- Database introduction
- Java/C/C++/Python Language
- ASP Web development
- ...
